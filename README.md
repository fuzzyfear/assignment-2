# assignment-2

#

# The assignment is unclear about the get a loan button.

# It says two things which contradict each other.

# Condition1: You cannot get more than one bank loan before buying a computer

# Condition2: Once you have a loan, you must pay it back BEFORE getting

# another loan

#

# The first condition implies you should be able to buy a new computer

# without having to pay back your loan.

# The second condition states that you are under no circumstances allowed to

# have more than one loan at the time.

#

# It is impossible to satisfy both conditions.

# I have chosen to only implement the first condition and ignore the second one
