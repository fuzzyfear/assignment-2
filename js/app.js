//Setup attributes
let money = 200;
let loan = 0;
let salary = 0;
let hasBoughtComputer = false;
let computers = [];

//Setup elements
const moneyElement = document.getElementById("money");
const loanElement = document.getElementById("loan");
const salaryElement = document.getElementById("salary");

const computersElement = document.getElementById("computers");
const titleElement = document.getElementById("title");
const priceElement = document.getElementById("price");
const specs1Element = document.getElementById("specs1");
const specs2Element = document.getElementById("specs2");
const descriptionElement = document.getElementById("description");
const imageElement = document.getElementById("image");

const loanRow1Element = document.getElementById("loan-row-p1");
const loanRow2Element = document.getElementById("loan-row-p2");
const repayLoanButtonElement = document.getElementById("repay-loan-button");

const getALoanButton = document.getElementById("get-a-loan-button");
const workButton = document.getElementById("work-button");
const bankButton = document.getElementById("bank-button");
const repayLoanButton = document.getElementById("repay-loan-button");
const buyNowButton = document.getElementById("buy-now-button");

//Select laptop setup
const handleComputerChange = (c) => {
  const selectedComputer = computers[c.target.selectedIndex];
  titleElement.innerHTML = selectedComputer.title;
  priceElement.innerHTML = selectedComputer.price;
  specs1Element.innerHTML = selectedComputer.specs[0];
  specs2Element.innerHTML = selectedComputer.specs[1];
  descriptionElement.innerHTML = selectedComputer.description;
  imageElement.src =
    "https://noroff-komputer-store-api.herokuapp.com/" + selectedComputer.image;
};
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
  .then((response) => response.json())
  .then((data) => (computers = data))
  .then((computers) => addComputersToStore(computers));
const addComputersToStore = (computers) => {
  computers.forEach((c) => addComputerToStore(c));
  computers[4].image = "assets/images/5.png";
  titleElement.innerHTML = computers[0].title;
  priceElement.innerHTML = computers[0].price;
  specs1Element.innerHTML = computers[0].specs[0];
  specs2Element.innerHTML = computers[1].specs[1];
  descriptionElement.innerHTML = computers[0].description;
  imageElement.src =
    "https://noroff-komputer-store-api.herokuapp.com/" + computers[0].image;
};
const addComputerToStore = (computer) => {
  const computerElement = document.createElement("option");
  computerElement.value = computer.id;
  computerElement.appendChild(document.createTextNode(computer.title));
  computersElement.appendChild(computerElement);
};

//Set values in UI at start
moneyElement.innerHTML = money;
loanElement.innerHTML = loan;
salaryElement.innerHTML = salary;

//UI interactions
getALoanButton.addEventListener("click", getALoan);
workButton.addEventListener("click", work);
bankButton.addEventListener("click", bank);
repayLoanButton.addEventListener("click", repayLoan);
buyNowButton.addEventListener("click", buyNow);
computersElement.addEventListener("change", handleComputerChange);

//Hide loan UI at start
hideOrShowLoan();

//Hide or show loan
function hideOrShowLoan() {
  if (loan === 0) {
    loanRow1Element.style.visibility = "hidden";
    loanRow2Element.style.visibility = "hidden";
    repayLoanButtonElement.style.visibility = "hidden";
  } else {
    loanRow1Element.style.visibility = "visible";
    loanRow2Element.style.visibility = "visible";
    repayLoanButtonElement.style.visibility = "visible";
  }
}

//Account-name section
//The user can get a loan.
function getALoan() {
  let newLoan = prompt(
    "Please enter the amount of money you wish to loan",
    100
  );
  if (newLoan > 0) {
    if (newLoan > money * 2) {
      alert("The loan is too high");
    } else if (loan > 0 && hasBoughtComputer === false) {
      alert("You already have a loan. Pay it back or buy a computer first!");
    } else {
      alert(`Loan conditions ok. Yoy have loaned: ${newLoan} Kr`);
      hasBoughtComputer = false;
      loan += +newLoan;
      loanElement.innerHTML = loan;
      hideOrShowLoan();
      money += +loan;
      moneyElement.innerHTML = money;
    }
  }
}
//Work section
//The user can bank their earned money.
function bank() {
  if (salary > 0) {
    if (loan > 0) {
      loan -= salary * 0.1;
      salary *= 0.9;
      if (loan < 0) {
        salary += Math.abs(loan);
        loan = 0;
      }
      loanElement.innerHTML = loan;
      hideOrShowLoan();
    }
    money += salary;
    moneyElement.innerHTML = money;
    salary = 0;
    salaryElement.innerHTML = salary;
  }
}
//The user can work to earn money.
function work() {
  salary += 100;
  salaryElement.innerHTML = salary;
}
//If the user has a loan, the user can pay back the loan from their payment.
function repayLoan() {
  if (loan > 0) {
    loan -= salary;
    salary = 0;
    if (loan < 0) {
      salary += Math.abs(loan);
      loan = 0;
    }
    hideOrShowLoan();
    loanElement.innerHTML = loan;
    salaryElement.innerHTML = salary;
  }
}

//Buy-now section
//The user can buy the selected computer
function buyNow() {
  price = priceElement.innerHTML;
  if (money < price) {
    alert("You don't have enough money to buy this computer");
  } else {
    hasBoughtComputer = true;
    money -= price;
    moneyElement.innerHTML = money;
    alert("You have boguht a new computer");
  }
}
